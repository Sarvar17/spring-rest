package edu.epam.fop.rest.controller;

import edu.epam.fop.rest.model.Role;
import edu.epam.fop.rest.model.User;
import edu.epam.fop.rest.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    // public endpoint
    @PostMapping("/users")
    public User createUser(@RequestBody User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }

    // admin endpoint
    @PutMapping("/users/{id}/roles")
    public User updateUserRoles(@PathVariable Long id, @RequestBody Set<Role> roles) {
        User user = userRepository.findById(id).orElseThrow(() -> new RuntimeException("User not found"));
        user.setRoles(roles);
        return userRepository.save(user);
    }

    // private endpoint
    @GetMapping("/me")
    public User getUser(@AuthenticationPrincipal UserDetails userDetails) {
        return userRepository.findByUsername(userDetails.getUsername());
    }

    // private endpoint
    @PutMapping("/me")
    public User updateUser(@AuthenticationPrincipal UserDetails userDetails, @RequestBody User updatedUser) {
        User user = userRepository.findByUsername(userDetails.getUsername());
        user.setUsername(updatedUser.getUsername());
        if (updatedUser.getPassword() != null && !updatedUser.getPassword().isEmpty()) {
            user.setPassword(passwordEncoder.encode(updatedUser.getPassword()));
        }
        return userRepository.save(user);
    }
}
