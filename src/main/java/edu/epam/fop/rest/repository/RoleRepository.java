package edu.epam.fop.rest.repository;

import edu.epam.fop.rest.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
}
